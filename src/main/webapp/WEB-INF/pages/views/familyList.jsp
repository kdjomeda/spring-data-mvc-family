<%-- 
    Document   : familyList
    Created on : Feb 16, 2014, 7:50:41 PM
    Author     : joseph
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to family page</title>
    </head>
    <body>
        <h1>Family List</h1>
        <table border="1">
                <tr>
                    <td>name</td>
                    <td>tribe</td>
                    <td>description</td>
                    <td colspan="3">Action</td>
                </tr>
                <c:forEach items="${families}" var="family">
                <tr>
                    <td>${family.name}</td>
                    <td>${family.tribe}</td>
                    <td>${family.description}</td>
                    <td><a href="<spring:url value="/family/edit/${family.ID}" />">Edit</a></td>
                    <td><a href="<spring:url value="/family/delete/${family.ID}" />">Delete</a></td>
                    <td><a href="<spring:url value="#" />">show members</a></td>
                </tr>
            </c:forEach> 

           
        </table>
                        
            <br/>
             <a href="<spring:url value="/"/>">Home</a> <a href="<spring:url value="/family/create"/>">Add</a>
    </body>
</html>
