<%-- 
    Document   : familyOne
    Created on : Feb 16, 2014, 7:51:05 PM
    Author     : joseph
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Family Page</title>
        <style type="text/css">

        </style>
    </head>
    <body>
        <h3>Welcome to ${family.name} 's family </h3>
        <br/>
        <div>
            <div style="float: left">Name</div><div style="float: right">${family.name}</div>
        </div>
        <div>
            <div style="float: left">Tribe</div><div style="float: right">${family.tribe}</div>
        </div>
        <div>
            <div style="float: left">Description</div><div style="float: right">${family.description}</div>
        </div>
        <br/>
        <a href="<spring:url value="/" />">Home</a> <a href="#" onclick="history.back()">Back</a>
    </body>
</html>
