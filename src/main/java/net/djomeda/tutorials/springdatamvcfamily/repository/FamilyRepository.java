package net.djomeda.tutorials.springdatamvcfamily.repository;

import net.djomeda.tutorials.springdatamvcfamily.model.Family;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author joseph
 */
public interface FamilyRepository extends JpaRepository<Family, String> {
    
}
