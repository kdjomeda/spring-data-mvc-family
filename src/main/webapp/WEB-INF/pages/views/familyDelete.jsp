<%-- 
    Document   : familyDelete
    Created on : Feb 17, 2014, 12:20:26 PM
    Author     : joseph
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Family Delete Page</title>
    </head>
    <body>
        <h3>${msg}</h3>
        
        <a href="<spring:url value="/" />">Home</a> <a href="#" onclick="history.back();">Back</a>
    </body>
</html>
