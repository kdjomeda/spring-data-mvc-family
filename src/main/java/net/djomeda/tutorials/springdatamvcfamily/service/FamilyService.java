package net.djomeda.tutorials.springdatamvcfamily.service;

import java.util.List;
import java.util.UUID;
import net.djomeda.tutorials.springdatamvcfamily.model.Family;
import net.djomeda.tutorials.springdatamvcfamily.repository.FamilyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author joseph
 */
@Service
public class FamilyService {
    
    @Autowired
    FamilyRepository familyRepository;
    /**
     * Method to pull all records from the database
     * @return List of Family Objects
     */
    public List<Family> findAll(){
        return familyRepository.findAll();
    }
    
    /**
     * Method to pull records from the database page by page with the help of the number of records per page.
     * @param pageSize total number of records per page.
     * @param pageNumber number of page
     * @return Page Object from Spring
     */
    public Page findAll(int pageSize, int pageNumber){
        return familyRepository.findAll(new PageRequest(pageNumber, pageSize));
    }
    
    /**
     * Method to query by Id
     * @param Id of the object to search 
     * @return queried Family Object
     */
    public Family findById(String Id){
      return familyRepository.findOne(Id);
    }
    
    /**
     * Method to persist a family Object
     * @param family object to persist
     * @return saved Family object 
     */
    public Family save(Family family){
        return familyRepository.save(family);
    }
    
    /**
     * Method to persist a family Object by creating  the Family Object before hand, taking parameters like name, tribe ,description
     * @param name family name
     * @param tribe family tribe
     * @param description family description
     * @return the newly persisted family object 
     */
    public Family create(String name, String tribe, String description){
        Family newFamily = new Family();
        newFamily.setID(UUID.randomUUID().toString());
        newFamily.setName(name);
        newFamily.setTribe(tribe);
        newFamily.setDescription(description);
        
        return familyRepository.save(newFamily);
    }
    
    /**
     * Method to delete a particular object by its Id
     * @param Id Family object to delete
     */
    public void delete(String Id){
        familyRepository.delete(Id);
    }
   
    /**
     * Method to delete a particular object
     * @param family object to delete
     */
    public void delete(Family family){
        familyRepository.delete(family);
    }
    
}
