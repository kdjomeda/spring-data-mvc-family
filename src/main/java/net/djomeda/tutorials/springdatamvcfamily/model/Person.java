package net.djomeda.tutorials.springdatamvcfamily.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author joseph
 */
@Entity
@Table(name = "person")
public class Person {
    
    @Id
    @Column(name = "person_id")
    private String ID;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "age")
    private String age;
    
    @Column(name = "gender")
    private String gender;
    
    @ManyToOne
    @JoinColumn(name = "family_id")
    private Family family;

    @OneToOne(cascade = CascadeType.ALL)
    private Profile profile;
    /**
     * @return the ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the age
     */
    public String getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(String age) {
        this.age = age;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }
    
    
}
