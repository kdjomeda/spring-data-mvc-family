#Very simplistic README

Spring Data MVC Family
======================

Introduction
------------

This project is to show case the usage of spring to perform basic CRUD and have a good understanding of how the stack Spring , Maven, Hibernate, JSTL could
be use to do a project the java way.


Overview
--------

This project uses the anime Naruto for illustration. There is the use of models like Family, Person and Profile. The use of family is depicted by 
all the clan and families in the leaf village and how people fall under one classification or the other based on the clans they belong to. 
The main point is to have a OneToMany and OneToOne relationship coverded and well diplayed with some technics from JSTL taglibs.

  

